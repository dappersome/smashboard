import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatSelectModule } from '@angular/material/select';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatDividerModule } from '@angular/material/divider';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatListModule } from '@angular/material/list';
import { MatRippleModule } from '@angular/material/core';
import { MatCardModule } from '@angular/material/card';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatMenuModule } from '@angular/material/menu';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatDialogModule } from '@angular/material/dialog';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatRadioModule } from '@angular/material/radio';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatTabsModule } from '@angular/material/tabs';
import { MatButtonToggleModule } from '@angular/material/button-toggle';

import { appRoutes } from './modules/routing.config';
import { AppComponent } from './app.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { ScoreboardComponent } from './components/scoreboard/scoreboard.component';
import { PlayerScoreComponent } from './components/scoreboard/player-score/player-score.component';
import { ManageScoreboardComponent } from './components/manage-scoreboard/manage-scoreboard.component';
import { ManagePlayersComponent } from './components/manage-players/manage-players.component';
import { CharacterSelectComponent } from './components/character-select/character-select.component';
import { PlayersListComponent } from './components/manage-players/players-list/players-list.component';
import { PlayerDetailsComponent } from './components/manage-players/player-details/player-details.component';
import { PlayerNamePipe } from './modules/player-fullname.pipe';
import { LiveSetPreviewComponent } from './components/live-set-preview/live-set-preview.component';
import { ManageCharactersComponent } from './components/manage-characters/manage-characters.component';
import { ImagePathPipe } from './modules/image-path.pipe';
import { DialogComponent } from './components/dialog/dialog.component';
import { ManageStagesComponent } from './components/manage-stages/manage-stages.component';
import { StageTypePipe } from './modules/stage-type.pipe';
import { UploadComponent } from './components/upload/upload.component';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { AuthGuardService } from './modules/auth-guard/auth-guard.service';
import { NotificationService } from './services/notification/notification.service';
import { AuthenticationService } from './services/authentication/authentication.service';
import { UserService } from './services/user/user.service';
import { JwtInterceptor } from './modules/jwt-interceptor/jwt-interceptor.service';
import { ErrorInterceptorService } from './modules/error-interceptor/error-interceptor.service';
import { AppLayoutComponent } from './ui-components/app-layout/app-layout.component';
import { AppContentComponent } from './ui-components/app-content/app-content.component';
import { AppHeaderComponent } from './ui-components/app-header/app-header.component';
import { CreatePlayerComponent } from './components/manage-players/create-player/create-player.component';
import { UserRoleIconComponent } from './ui-components/user-role-icon/user-role-icon.component';
import { StageDetailsComponent } from './components/manage-stages/stage-details/stage-details.component';
import { StagesListComponent } from './components/manage-stages/stages-list/stages-list.component';
import { ManageGamesComponent } from './components/manage-games/manage-games.component';

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    ScoreboardComponent,
    PlayerScoreComponent,
    ManageScoreboardComponent,
    ManagePlayersComponent,
    CharacterSelectComponent,
    PlayersListComponent,
    PlayerDetailsComponent,
    PlayerNamePipe,
    LiveSetPreviewComponent,
    ManageCharactersComponent,
    ImagePathPipe,
    DialogComponent,
    ManageStagesComponent,
    StageTypePipe,
    UploadComponent,
    HomeComponent,
    LoginComponent,
    AppLayoutComponent,
    AppContentComponent,
    AppHeaderComponent,
    CreatePlayerComponent,
    UserRoleIconComponent,
    StageDetailsComponent,
    StagesListComponent,
    ManageGamesComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes),
    HttpClientModule,
    BrowserAnimationsModule,
    MatSelectModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatIconModule,
    MatToolbarModule,
    MatDividerModule,
    MatListModule,
    FormsModule,
    ReactiveFormsModule,
    MatRippleModule,
    MatCardModule,
    MatSnackBarModule,
    MatSidenavModule,
    MatMenuModule,
    MatGridListModule,
    MatDialogModule,
    MatCheckboxModule,
    MatRadioModule,
    MatProgressBarModule,
    MatExpansionModule,
    MatProgressSpinnerModule,
    MatTabsModule,
    MatButtonToggleModule
  ],
  providers: [
    PlayerNamePipe,
    AuthGuardService,
    NotificationService,
    AuthenticationService,
    UserService,
    ImagePathPipe,
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ErrorInterceptorService,
      multi: true
    }
  ],
  exports: [RouterModule],
  bootstrap: [AppComponent],
  entryComponents: [DialogComponent]
})
export class AppModule {}
