import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-user-role-icon',
  templateUrl: './user-role-icon.component.html',
  styleUrls: ['./user-role-icon.component.scss']
})
export class UserRoleIconComponent {
  iconMap = {
    admin: 'looks_one',
    moderator: 'looks_two',
    user: 'looks_3',
    player: 'sports_esports'
  };

  @Input()
  set userrole(role: string) {
    this.icon = this.iconMap[role] || this.iconMap.user;
  }

  icon: string;

  constructor() {}
}
