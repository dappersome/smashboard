import { Routes } from '@angular/router';

import { ManagePlayersComponent } from '../components/manage-players/manage-players.component';
import { ManageCharactersComponent } from '../components/manage-characters/manage-characters.component';
import { ManageScoreboardComponent } from '../components/manage-scoreboard/manage-scoreboard.component';
import { ManageStagesComponent } from '../components/manage-stages/manage-stages.component';
import { HomeComponent } from '../components/home/home.component';
import { AuthGuardService } from './auth-guard/auth-guard.service';
import { LoginComponent } from '../components/login/login.component';
import { ManageGamesComponent } from '../components/manage-games/manage-games.component';

export const appRoutes: Routes = [
  {
    path: '',
    component: HomeComponent,
    canActivate: [AuthGuardService],
    children: [
      {
        path: '',
        component: ManagePlayersComponent
      },
      {
        path: 'characters',
        component: ManageCharactersComponent
      },
      {
        path: 'scoreboard',
        component: ManageScoreboardComponent
      },
      {
        path: 'stages',
        component: ManageStagesComponent
      },
      {
        path: 'games',
        component: ManageGamesComponent
      }
    ]
  },
  {
    path: 'login',
    component: LoginComponent
  },
  { path: '**', redirectTo: '' }
];
