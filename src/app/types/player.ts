export interface Player {
  _id: string;
  playername: string;
  teamname?: string;
  character?: string;
}

export interface GamePlayer extends Player {
  score: number;
}
