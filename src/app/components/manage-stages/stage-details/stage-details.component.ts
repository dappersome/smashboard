import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Subscription, Subscriber } from 'rxjs';

import { Stage, StageType } from 'src/app/types';
import { StagesService } from 'src/app/services/stages/stages.service';
import { skip } from 'rxjs/operators';

@Component({
  selector: 'app-stage-details',
  templateUrl: './stage-details.component.html',
  styleUrls: ['./stage-details.component.scss']
})
export class StageDetailsComponent implements OnInit, OnDestroy {
  private subscription: Subscription;

  @Input() set stage(stageId: string) {
    this.stagesService.getStage(stageId);
  }

  stageTypes: StageType[] = [
    { name: 'Starter', type: 'starter' },
    { name: 'Counterpick', type: 'counter' }
  ];

  stageDetails$ = this.stagesService.stage$;

  updateStageForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private stagesService: StagesService
  ) {
    this.updateStageForm = this.formBuilder.group({
      _id: ['', [Validators.required]],
      name: ['', [Validators.required]],
      image: [{ value: '', disabled: true }, [Validators.required]],
      type: ['', [Validators.required]],
      roster: ['']
    });
  }

  update(): void {
    console.log(this.updateStageForm.value);
    this.stagesService.updateStage(this.updateStageForm.value);
  }

  ngOnInit() {
    this.subscription = this.stagesService.stage$
      .pipe(skip(1))
      .subscribe(stage => {
        this.updateStageForm.patchValue(stage);
      });
  }
  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
