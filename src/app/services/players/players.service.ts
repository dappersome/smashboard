import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';

import { Player, ApiData } from '../../types';
import { NotificationService } from '../notification/notification.service';
import { PlayerNamePipe } from 'src/app/modules/player-fullname.pipe';
import { ApiService } from '../api/api.service';
import { DialogOpenService } from 'src/app/helpers/dialog-open.helper';

@Injectable({
  providedIn: 'root'
})
export class PlayersService {
  apiType = 'players';

  private readonly playersSubject = new BehaviorSubject<Player[]>([]);
  players$ = this.playersSubject.asObservable();

  constructor(
    private notificationService: NotificationService,
    private api: ApiService,
    private playerNamePipe: PlayerNamePipe,
    private dialogService: DialogOpenService
  ) {}

  getPlayers(): void {
    this.api
      .getAll(this.apiType)
      .pipe(map((res: ApiData) => res.data.sort(this._sortByName)))
      .subscribe(
        (players: Player[]) => {
          this.playersSubject.next(players);
        },
        err => {
          this.notificationService.notify(`Couldn't fetch players.`, 'OK');
        }
      );
  }

  updatePlayer(player: Player): void {
    this.api.updateSingle(this.apiType, player).subscribe(
      () => {
        this.getPlayers();
        this.notificationService.notify(
          `"${this.playerNamePipe.transform(
            player.playername,
            player.teamname
          )}" has been updated.`,
          'OK'
        );
      },
      err => {
        this.notificationService.notify(`Whoops! ${err}`, `OK`);
      }
    );
  }

  /** POST: add a new player to the db */
  addPlayer(player: Player): void {
    this.api.createSingle(this.apiType, player).subscribe((res: Player) => {
      this.getPlayers();
      this.notificationService.notify(
        `New player "${this.playerNamePipe.transform(
          player.playername,
          player.teamname
        )}" has been added.`,
        'OK'
      );
    });
  }

  /** DELETE: delete the player from the db */
  deletePlayer(player: Player): void {
    const dialogDescription = `Confirm deleting ${player.playername}`;
    const dialogMessage = `Are you sure you want to delete ${player.playername}`;

    this.dialogService
      .open(dialogDescription, dialogMessage)
      .subscribe((responseUser: boolean) => {
        if (responseUser) {
          this.api.deleteSingle(this.apiType, player._id).subscribe(data => {
            this.getPlayers();
            this.notificationService.notify(
              `"${this.playerNamePipe.transform(
                player.playername,
                player.teamname
              )}" has been deleted.`,
              'OK'
            );
          });
        }
      });
  }

  _sortByName(a, b): any {
    if (a.playername < b.playername) {
      return -1;
    }
    if (a.playername > b.playername) {
      return 1;
    }
    return 0;
  }
}
