import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { Player } from '../../../types';

@Component({
  selector: 'app-player-details',
  templateUrl: './player-details.component.html',
  styleUrls: ['./player-details.component.scss']
})
export class PlayerDetailsComponent implements OnInit {
  @Input()
  set player(player: Player) {
    this.selectedPlayer = player;
    this.updatePlayerForm.patchValue(player);
  }
  selectedPlayer: Player;

  updatePlayerForm: FormGroup;

  constructor(private formBuilder: FormBuilder) {
    this.updatePlayerForm = this.formBuilder.group({
      _id: ['', [Validators.required]],
      teamname: ['', [Validators.required]],
      playername: ['', [Validators.required]],
      character: ['']
    });
  }

  ngOnInit() {}

  update(): void {
    console.log(this.updatePlayerForm.value);

    // const player = playerDetailsForm.form.value;

    // this.playersService.updatePlayer(player);
  }

  close(): void {
    this.selectedPlayer = undefined;
  }
}
