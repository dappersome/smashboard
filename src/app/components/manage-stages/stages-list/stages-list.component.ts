import { Component, OnInit, Output, EventEmitter } from '@angular/core';

import { Stage } from 'src/app/types';
import { StagesService } from 'src/app/services/stages/stages.service';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-stages-list',
  templateUrl: './stages-list.component.html',
  styleUrls: ['./stages-list.component.scss']
})
export class StagesListComponent implements OnInit {
  @Output() selectStage = new EventEmitter<string>();
  private selectedStageSubject = new BehaviorSubject<string | undefined>(
    undefined
  );
  selectedStage$ = this.selectedStageSubject.asObservable();

  stagesList$ = this.stagesService.stagesList$;

  constructor(private stagesService: StagesService) {}

  ngOnInit() {
    this.stagesService.getStages();
  }

  select(stageId: string): void {
    this.selectedStageSubject.next(stageId);
    this.selectStage.emit(stageId);
  }

  delete(event: Event, stage: Stage): void {
    event.stopPropagation();
    this.stagesService.delete(stage);
  }
}
