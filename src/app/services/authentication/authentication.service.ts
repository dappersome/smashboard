import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';

import { environment } from 'src/environments/environment';
import { User, NewUser, ApiUser } from 'src/app/types';
import { AuthGuardService } from 'src/app/modules/auth-guard/auth-guard.service';
import { ApiService } from '../api/api.service';
import { NotificationService } from '../notification/notification.service';

@Injectable()
export class AuthenticationService {
  private apiType = '';
  private apiUrl = `${environment.apiUrl}`; // URL to web api

  private readonly currentLoggedInUserSubject = new BehaviorSubject<
    any | undefined
  >(undefined);
  currentLoggedInUser$ = this.currentLoggedInUserSubject.asObservable();

  constructor(
    private http: HttpClient,
    private router: Router,
    private readonly authGuardService: AuthGuardService,
    private api: ApiService,
    private notificationService: NotificationService
  ) {}

  login(username: string, password: string) {
    return this.http
      .post<any>(`${this.apiUrl}/login`, {
        username,
        password
      })
      .pipe(
        map(user => {
          // login successful if there's a jwt token in the response
          if (user && user.accessToken) {
            // store user details and jwt token in local storage to keep user logged in between page refreshes
            localStorage.setItem('currentUser', JSON.stringify(user));
            this.currentLoggedInUserSubject.next(user);
          }

          return user;
        })
      );
  }

  logout(redirect?: string) {
    // remove user from local storage to log user out
    localStorage.removeItem('currentUser');
    if (redirect) {
      this.router.navigate([redirect]);
    }
  }

  register(user: NewUser) {
    const { username, password, teamname, playername } = user;

    return this.http.post<any>(`${this.apiUrl}/register`, {
      username,
      password,
      playername,
      teamname
    });
  }
}
