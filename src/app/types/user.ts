export interface NewUser {
  username: string;
  teamname: string;
  playername: string;
  password: string;
  _id?: string;
}

export interface User {
  _id: string;
  username: string;
  role: string;
  teamname: string;
  playername: string;
}
