import { Stage } from './stage';
import { User, NewUser } from './user';
import { Character } from './character';
import { Game } from './game';
import { Player } from './player';

export interface ApiData {
  data: [];
}
export interface ApiSingleData {
  data: ApiItem;
}
export interface ApiUser extends ApiData {
  accessToken: string;
}

export type ApiItem = Character | Stage | Game | NewUser | Player;
