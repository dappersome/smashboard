import { Component, Input } from '@angular/core';

import { AuthenticationService } from 'src/app/services/authentication/authentication.service';
import { AppConfig } from 'src/app/modules/app.config';
import { UserService } from 'src/app/services/user/user.service';

@Component({
  selector: 'app-header',
  templateUrl: './app-header.component.html',
  styleUrls: ['./app-header.component.scss']
})
export class AppHeaderComponent {
  @Input() userName: string;

  app$ = AppConfig;

  constructor(
    private readonly authenticationService: AuthenticationService,
    private readonly userService: UserService
  ) {}

  logout(): void {
    this.authenticationService.logout('/login');
  }
}
