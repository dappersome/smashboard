import { Injectable } from '@angular/core';
import { Subject, Observable, merge, Subscription } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { map } from 'rxjs/operators';

import { NewUser, User } from 'src/app/types';
import { NotificationService } from 'src/app/services/notification/notification.service';
import { DialogComponent } from 'src/app/components/dialog/dialog.component';
import { ApiService } from '../api/api.service';
import { AuthGuardService } from 'src/app/modules/auth-guard/auth-guard.service';
import { AuthenticationService } from '../authentication/authentication.service';

@Injectable()
export class UserService {
  private subscription: Subscription;
  apiType = 'users';
  storageCurrentUser: any;

  private readonly userListSubject = new Subject<User[]>();
  userList$ = this.userListSubject.asObservable();

  private readonly selectedUserSubject = new Subject<NewUser>();
  selectedUser$ = this.selectedUserSubject.asObservable();

  private statusResponseSubject = new Subject();
  statusResponse$ = this.statusResponseSubject.asObservable();

  constructor(
    private notificationService: NotificationService,
    private dialog: MatDialog,
    private api: ApiService,
    private authGuardService: AuthGuardService,
    private authenticationService: AuthenticationService
  ) {}

  getUsers(): void {
    this.api
      .getAll(this.apiType)
      .pipe(map(res => res.data))
      .subscribe(
        (users: User[]) => {
          this.userListSubject.next(users);
        },
        err => {
          this.notificationService.notify(`Couldn't fetch users.`, `OK`);
        }
      );
  }

  getUser(user: User): void {
    this.api
      .getSingle(this.apiType, user._id)
      .pipe(map(res => res.data))
      .subscribe(
        (res: any) => {
          this.selectedUserSubject.next(res);
        },
        err => {
          this.notificationService.notify(
            `Something went wrong fetching this user`,
            `OK`
          );
        }
      );
  }

  update(user: NewUser): void {
    this.api.updateSingle(this.apiType, user).subscribe(
      () => {
        this.getUsers();
        this.notificationService.notify(
          `Profile has been updated successfully.`,
          `OK`
        );
      },
      err => {
        this.notificationService.notify(`Whoops! ${err}`, `OK`);
      }
    );
  }

  delete(user: User): void {
    const dialogDescription = `Confirm deleting '${user.username}'`;
    const dialogMessage = `Are you sure you want to delete '${user.username}'`;

    this._openDialog(dialogDescription, dialogMessage).subscribe(
      (responseUser: boolean) => {
        if (responseUser) {
          this.api
            .deleteSingle(this.apiType, user._id)
            .subscribe((data: any) => {
              this.notificationService.notify(data.message, 'OK');
              this.getUsers();
            });
        }
      }
    );
  }

  private _openDialog(description: string, message: string): Observable<any> {
    const dialogRef = this.dialog.open(DialogComponent, {
      data: {
        description,
        message
      }
    });

    return dialogRef.afterClosed();
  }
}
