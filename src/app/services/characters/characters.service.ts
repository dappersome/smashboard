import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { MatDialog } from '@angular/material/dialog';

import { Character } from 'src/app/types';
import { NotificationService } from 'src/app/services/notification/notification.service';
import { DialogComponent } from 'src/app/components/dialog/dialog.component';
import { ApiService } from '../api/api.service';

@Injectable({
  providedIn: 'root'
})
export class CharactersService {
  apiType = 'characters';
  private readonly charactersListSubject = new BehaviorSubject<
    Character[] | undefined
  >(undefined);
  charactersList$ = this.charactersListSubject.asObservable();

  private readonly fetchedCharacterSubject = new BehaviorSubject<
    Character | undefined
  >(undefined);
  fetchedCharacter$ = this.fetchedCharacterSubject.asObservable();

  constructor(
    private notificationService: NotificationService,
    private dialog: MatDialog,
    private api: ApiService
  ) {
    this.getCharacters();
  }

  create(character: Character): void {
    this.api
      .createSingle(this.apiType, character)
      .subscribe((res: Character) => {
        this.getCharacters();
        this.notificationService.notify(
          `New character "${res.name}" has been created.`,
          'OK'
        );
      });
  }

  createBulk(characterCSV: any): void {
    const lines = characterCSV.split('\n');
    const bulkArray: Character[] = [];

    for (const charLine of lines) {
      const charLineValues = charLine.split(',');
      bulkArray.push({
        name: charLineValues[0],
        image: charLineValues[1]
      });
    }
    this.api
      .createMultiple(this.apiType, bulkArray)
      .subscribe((res: Character) => {
        this.notificationService.notify(
          `New characters have been created.`,
          'OK'
        );
        this.getCharacters();
      });
  }

  getCharacters(): void {
    this.api
      .getAll(this.apiType)
      .pipe(map(characters => characters.data.sort(this._sortByRoster)))
      .subscribe(
        (res: Character[]) => {
          res.map(character => {
            character.styles = {
              'background-image': `url(/assets/uploads/${character.image})`
            };
          });
          this.charactersListSubject.next(res);
        },
        err => {
          this.notificationService.notify(
            `Couldn't fetch characters.`,
            'Dismiss'
          );
        }
      );
  }

  getCharacter(character: Character) {
    this.api
      .getSingle(this.apiType, character._id)
      .pipe(map(res => res.data))
      .subscribe((res: any) => {
        this.fetchedCharacterSubject.next(res);
      });
  }

  update(character: Character): void {
    this.api
      .updateSingle(this.apiType, character)
      .subscribe((res: Character) => {
        this.getCharacters();
        this.notificationService.notify(
          `"${res.name}" has been updated.`,
          'OK'
        );
      });
  }

  delete(character: Character): void {
    const dialogDescription = `Confirm deleting ${character.name}`;
    const dialogMessage = `Are you sure you want to delete ${character.name}`;

    this._openDialog(dialogDescription, dialogMessage).subscribe(
      (responseUser: boolean) => {
        if (responseUser) {
          this.api
            .deleteSingle(this.apiType, character._id)
            .subscribe((res: Character) => {
              this.getCharacters();
              this.notificationService.notify(
                `"${character.name}" has been deleted.`,
                'OK'
              );
            });
        }
      }
    );
  }

  private _sortByRoster(a, b): any {
    if (a.roster < b.roster) {
      return -1;
    }
    if (a.roster > b.roster) {
      return 1;
    }
    return 0;
  }

  private _openDialog(description: string, message: string): Observable<any> {
    const dialogRef = this.dialog.open(DialogComponent, {
      data: {
        description,
        message
      }
    });

    return dialogRef.afterClosed();
  }
}
