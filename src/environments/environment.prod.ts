export const environment = {
  production: true,
  apiUrl: 'http://smash.markdejong.me:8080',
  uploadsDir: '/assets/uploads/'
};
