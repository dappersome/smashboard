import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { environment } from 'src/environments/environment';
import {
  ApiData,
  ApiItem,
  Character,
  Stage,
  NewUser,
  Game,
  ApiSingleData
} from 'src/app/types';
import { ErrorHandlerService } from 'src/app/helpers/error-handler.helper';
import { AuthGuardService } from 'src/app/modules/auth-guard/auth-guard.service';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  private apiBaseUrl = `${environment.apiUrl}/api`;
  httpOptions = {};

  constructor(
    private readonly http: HttpClient,
    private readonly errorHandler: ErrorHandlerService,
    private readonly authGuardService: AuthGuardService
  ) {
    this.authGuardService.currentStoredUser$.subscribe(user => {
      if (user) {
        this.httpOptions = {
          headers: new HttpHeaders({
            'Content-Type': 'application/json',
            'x-access-token': user.accessToken
          })
        };
      } else {
        this.httpOptions = {
          headers: new HttpHeaders({
            'Content-Type': 'application/json'
          })
        };
      }
    });
  }

  createSingle(type: string, item: ApiItem): Observable<ApiItem> {
    return this.http
      .post<ApiItem>(
        `${this.apiBaseUrl}/${type}/create`,
        item,
        this.httpOptions
      )
      .pipe(catchError(this.errorHandler.handleError));
  }

  createMultiple(type: string, items: ApiItem[]): Observable<ApiItem> {
    return this.http
      .post<ApiItem>(
        `${this.apiBaseUrl}/${type}/create/bulk`,
        items,
        this.httpOptions
      )
      .pipe(catchError(this.errorHandler.handleError));
  }

  registerUser(type: string, item: ApiItem): Observable<any> {
    return this.http
      .post<ApiItem>(`${this.apiBaseUrl}/register`, item, this.httpOptions)
      .pipe(catchError(this.errorHandler.handleError));
  }

  getAll(type: string): Observable<ApiData> {
    return this.http
      .get<ApiData>(`${this.apiBaseUrl}/${type}`, this.httpOptions)
      .pipe(catchError(this.errorHandler.handleError));
  }

  getSingle(type: string, id: string): Observable<ApiSingleData> {
    return this.http
      .get<ApiSingleData>(`${this.apiBaseUrl}/${type}/${id}`, this.httpOptions)
      .pipe(catchError(this.errorHandler.handleError));
  }

  updateSingle(type: string, item: ApiItem): Observable<ApiItem> {
    return this.http
      .put<ApiItem>(
        `${this.apiBaseUrl}/${type}/${item._id}`,
        item,
        this.httpOptions
      )
      .pipe(catchError(this.errorHandler.handleError));
  }

  deleteSingle(type: string, id: string): Observable<ApiItem> {
    return this.http
      .delete<ApiItem>(`${this.apiBaseUrl}/${type}/${id}`, this.httpOptions)
      .pipe(catchError(this.errorHandler.handleError));
  }
}
