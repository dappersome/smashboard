import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { BehaviorSubject } from 'rxjs';

import { StagesService } from 'src/app/services/stages/stages.service';
import { Stage } from 'src/app/types';

@Component({
  selector: 'app-manage-stages',
  templateUrl: './manage-stages.component.html',
  styleUrls: ['./manage-stages.component.scss']
})
export class ManageStagesComponent {
  appHeader = 'Manage Stages';

  @ViewChild('stageName', { static: true }) stageNameRef: ElementRef;

  createStageForm: FormGroup;
  selectedStage$ = new BehaviorSubject<Stage | undefined>(undefined);

  constructor(
    private formBuilder: FormBuilder,
    private stagesService: StagesService
  ) {
    this.stagesService.getStages();
    this.createStageForm = this.formBuilder.group({
      name: new FormControl(''),
      image: new FormControl(''),
      type: new FormControl(''),
      roster: new FormControl('')
    });
  }

  create(): void {
    console.log(this.createStageForm.value);
    this.stagesService.create(this.createStageForm.value);
    this.createStageForm.reset();
    this.stageNameRef.nativeElement.focus();
  }

  selectStage(stage: Stage): void {
    this.selectedStage$.next(stage);
  }

  delete(stage: Stage): void {
    this.stagesService.delete(stage);
  }

  uploadFinished(uploadedFile: any) {
    this.createStageForm.patchValue({
      image: uploadedFile.file
    });
  }
}
