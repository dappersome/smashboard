import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { first } from 'rxjs/operators';

import { AuthenticationService } from 'src/app/services/authentication/authentication.service';
import { NotificationService } from 'src/app/services/notification/notification.service';
import { BehaviorSubject } from 'rxjs';

@Component({
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  registerForm: FormGroup;

  formRegisterState = {
    submitted: false,
    loading: false,
    disabled: false
  };
  formLoginState = {
    submitted: false,
    loading: false,
    disabled: false
  };
  returnUrl: string;
  selectedTab = new BehaviorSubject(0);

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService,
    private notificationService: NotificationService
  ) {}

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
    this.registerForm = this.formBuilder.group({
      username: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]],
      teamname: ['', [Validators.required]],
      playername: ['', [Validators.required]]
    });

    // reset login status
    this.authenticationService.logout();

    // get return url from route parameters or default to '/'
    this.returnUrl = this.route.snapshot.queryParams.returnUrl || '/';
  }

  onSubmitLogin() {
    this.formLoginState.submitted = true;

    // stop here if form is invalid
    if (this.loginForm.invalid) {
      return;
    }

    this.formLoginState.loading = true;
    this.authenticationService
      .login(this.loginForm.value.username, this.loginForm.value.password)
      .pipe(first())
      .subscribe(
        data => {
          this.router.navigate([this.returnUrl]);
        },
        error => {
          this.notificationService.notify(error, 'OK');
          this.formLoginState.loading = false;
        }
      );
  }

  onSubmitRegister(): void {
    // stop here if form is invalid
    if (this.registerForm.invalid) {
      return;
    }

    this.formRegisterState.submitted = true;
    this.formRegisterState.loading = true;

    this.authenticationService
      .register(this.registerForm.value)
      .pipe(first())
      .subscribe(
        res => {
          this.formRegisterState.submitted = true;
          this.formRegisterState.loading = false;
          this.formRegisterState.disabled = true;

          this.registerForm.reset();
          this.registerForm.disable();
          this.notificationService.notify(
            `You've been successfully registered!`,
            `OK`
          );
          this.selectedTab.next(0);
        },
        err => {
          this.formRegisterState.submitted = false;
          this.formRegisterState.loading = false;

          // this.registerForm.reset();
          this.notificationService.notify(`Whoops! ${err}`, `OK`);
        }
      );
  }
}
