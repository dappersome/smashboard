export interface StockIcon {
  name: string;
  character: string;
  iconFile: string;
}
