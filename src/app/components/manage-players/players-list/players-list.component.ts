import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  Output,
  EventEmitter
} from '@angular/core';
import { NgForm } from '@angular/forms';

import { Player, User, NewUser } from 'src/app/types';
import { UserService } from 'src/app/services/user/user.service';
import { UserProfileService } from 'src/app/services/user-profile/user-profile.service';
import { map } from 'rxjs/operators';
import { AuthenticationService } from 'src/app/services/authentication/authentication.service';
import { PlayersService } from 'src/app/services/players/players.service';

@Component({
  selector: 'app-players-list',
  templateUrl: './players-list.component.html',
  styleUrls: ['./players-list.component.scss']
})
export class PlayersListComponent implements OnInit {
  @ViewChild('playerName', { static: true }) playerNameRef: ElementRef;
  @ViewChild('playerTeam', { static: true }) playerTeamRef: ElementRef;

  @Output() selectPlayer = new EventEmitter<NewUser>();
  selectedPlayer: NewUser;

  currentUserIsAdmin$ = this.userProfileService.currentActiveUser$.pipe(
    map(currentUser =>
      currentUser && currentUser.role === 'admin' ? true : false
    )
  );

  players$ = this.playerService.players$;

  constructor(
    private userService: UserService,
    private userProfileService: UserProfileService,
    private authenticationService: AuthenticationService,
    private playerService: PlayersService
  ) {
    this.playerService.getPlayers();
  }

  ngOnInit() {
    this.userService.getUsers();
  }

  select(player: NewUser): void {
    this.selectedPlayer = player;
    this.selectPlayer.emit(player);
  }

  add(playerForm: NgForm): void {
    const player = playerForm.form.value;
    if (!player.name) {
      return;
    }
    player.name = player.name.trim();

    if (player.team) {
      player.team = player.team.trim();
    }
    this.authenticationService.register(player);
    this._resetForm(playerForm);
  }

  delete(event: Event, player: User): void {
    event.stopPropagation();
    this.userService.delete(player);
  }

  trackByPlayerId(index: number, player: Player) {
    if (!player) {
      return null;
    }

    return player._id;
  }

  private _resetForm(form: NgForm): void {
    form.resetForm();
    this.playerTeamRef.nativeElement.focus();
  }
}
