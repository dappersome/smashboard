import { Component, OnInit } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { NewUser } from 'src/app/types';

@Component({
  selector: 'app-manage-players',
  templateUrl: './manage-players.component.html',
  styleUrls: ['./manage-players.component.scss']
})
export class ManagePlayersComponent implements OnInit {
  appHeader = 'Manage Players';

  selectedPlayer$ = new BehaviorSubject<NewUser | undefined>(undefined);

  constructor() {}

  private _compareFn(a, b) {
    if (a.fullname < b.fullname) {
      return -1;
    }
    if (a.fullname > b.fullname) {
      return 1;
    }
    return 0;
  }

  ngOnInit() {}

  selectPlayer(player) {
    this.selectedPlayer$.next(player);
  }
}
