import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { SetType } from 'src/app/types';
import { PlayersService } from 'src/app/services/players/players.service';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-manage-games',
  templateUrl: './manage-games.component.html',
  styleUrls: ['./manage-games.component.scss']
})
export class ManageGamesComponent implements OnInit {
  appHeader = 'Manage Games';

  playersList$ = this.playersService.players$;

  setForm: FormGroup;
  gameItems: FormArray;

  get gameFormGroup() {
    return this.setForm.get('games') as FormArray;
  }

  setTypes: string[] = ['1', '3', '5'];

  constructor(
    private readonly fb: FormBuilder,
    private playersService: PlayersService
  ) {}

  ngOnInit() {
    this.playersService.getPlayers();

    this.setForm = this.fb.group({
      setType: [this.setTypes[1], [Validators.required]],
      games: this.fb.array([this.createGame()]),
      player1: this.fb.group({
        id: ['', [Validators.required]],
        score: [0, [Validators.required]]
      }),
      player2: this.fb.group({
        id: ['', [Validators.required]],
        score: [0, [Validators.required]]
      })
    });
    this.gameItems = this.setForm.get('games') as FormArray;
  }

  createGame(): FormGroup {
    return this.fb.group({
      player1: this.fb.group({
        id: [null, Validators.compose([Validators.required])],
        stocks: [null, Validators.compose([Validators.required])],
        character: [null, Validators.compose([Validators.required])]
      }),
      player2: this.fb.group({
        id: [null, Validators.compose([Validators.required])],
        stocks: [null, Validators.compose([Validators.required])],
        character: [null, Validators.compose([Validators.required])]
      }),
      stage: [null, Validators.compose([Validators.required])],
      winner: [null, Validators.compose([Validators.required])]
    });
  }

  addItem(gameItem) {
    this.gameItems.push(this.createGame());
    // this.games.push(this.fb.control(false));
  }
  removeItem(index) {
    // this.gameItems.pop();
    this.gameItems.removeAt(index);
  }

  getGameItemsFormGroup(index): FormGroup {
    return this.gameItems.controls[index] as FormGroup;
  }

  createSet() {
    console.log(this.setForm.value);
  }
}
