import { Injectable } from '@angular/core';
import { BehaviorSubject, merge } from 'rxjs';

import { AuthGuardService } from 'src/app/modules/auth-guard/auth-guard.service';
import { AuthenticationService } from 'src/app/services/authentication/authentication.service';
import { User } from 'src/app/types';
import { UserService } from '../user/user.service';

@Injectable({
  providedIn: 'root'
})
export class UserProfileService {
  private readonly currentActiveUserSubject = new BehaviorSubject<
    User | undefined
  >(undefined);
  currentActiveUser$ = this.currentActiveUserSubject.asObservable();

  constructor(
    private readonly authGuardService: AuthGuardService,
    private readonly authenticationService: AuthenticationService,
    private readonly userService: UserService
  ) {
    merge(
      this.authGuardService.currentStoredUser$,
      this.authenticationService.currentLoggedInUser$
    ).subscribe(results => {
      if (results) {
        this.currentActiveUserSubject.next(results.data);
      }
    });
  }
}
