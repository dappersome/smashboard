import { Injectable } from '@angular/core';
import {
  CanActivate,
  Router,
  ActivatedRouteSnapshot,
  RouterStateSnapshot
} from '@angular/router';
import { User } from 'src/app/types';
import { BehaviorSubject, Subject } from 'rxjs';

@Injectable()
export class AuthGuardService implements CanActivate {
  private readonly currentStoredUserSubject = new BehaviorSubject<
    any | undefined
  >(undefined);
  currentStoredUser$ = this.currentStoredUserSubject.asObservable();

  constructor(private router: Router) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (localStorage.getItem('currentUser')) {
      const currentUser: User = JSON.parse(localStorage.getItem('currentUser'));

      this.currentStoredUserSubject.next(currentUser);

      // logged in so return true
      return true;
    }

    // not logged in so redirect to login page with the return url
    this.router.navigate(['/login'], { queryParams: { returnUrl: state.url } });
    return false;
  }
}
