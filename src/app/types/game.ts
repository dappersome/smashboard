export interface SetType {
  type: string;
  name: string;
}

export interface GameSet {
  game: number;
  player1: string;
  player2: string;
  _id?: any;
  live: boolean;
}

export interface Game {
  type: string;
  [key: string]: string;
}
