import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { map, subscribeOn } from 'rxjs/operators';

import { Stage } from 'src/app/types';
import { NotificationService } from '../notification/notification.service';
import { ApiService } from '../api/api.service';
import { DialogOpenService } from 'src/app/helpers/dialog-open.helper';

@Injectable({
  providedIn: 'root'
})
export class StagesService {
  private readonly stagesListSubject = new BehaviorSubject<Stage[] | undefined>(
    undefined
  );
  stagesList$ = this.stagesListSubject.asObservable();
  private readonly stageSubject = new BehaviorSubject<Stage | undefined>(
    undefined
  );
  stage$ = this.stageSubject.asObservable();

  apiType = 'stages';

  constructor(
    private notificationService: NotificationService,
    private api: ApiService,
    private dialogService: DialogOpenService
  ) {}

  create(stage: Stage): void {
    this.api.createSingle(this.apiType, stage).subscribe((res: Stage) => {
      this.getStages();
      this.notificationService.notify(
        `New stage '${res.name}' has been created.`,
        'OK'
      );
    });
  }

  getStages(): void {
    this.api
      .getAll(this.apiType)
      .pipe(map(stages => stages.data.sort(this._sortByRoster)))
      .subscribe(
        (stages: Stage[]) => {
          stages.map(stage => {
            stage.styles = {
              'background-image': `url(/assets/uploads/${stage.image})`
            };
          });
          this.stagesListSubject.next(stages);
        },
        err => {
          this.notificationService.notify(`Couldn't fetch stages.`, 'OK');
        }
      );
  }

  getStage(stageId: string): void {
    this.api
      .getSingle(this.apiType, stageId)
      .pipe(map(res => res.data))
      .subscribe(
        (res: Stage) => {
          this.stageSubject.next(res);
        },
        err => {
          this.notificationService.notify(
            `Something went wrong getting Stage details.`,
            'OK'
          );
        }
      );
  }

  updateStage(stage: Stage): void {
    this.api.updateSingle(this.apiType, stage).subscribe((res: Stage) => {
      this.getStages();
      this.notificationService.notify(
        `Stage '${stage.name}' has been updated.`,
        'OK'
      );
    });
  }

  delete(stage: Stage): void {
    const dialogDescription = `Confirm deleting ${stage.name}`;
    const dialogMessage = `Are you sure you want to delete ${stage.name}`;

    this.dialogService
      .open(dialogDescription, dialogMessage)
      .subscribe((responseUser: boolean) => {
        if (responseUser) {
          this.api
            .deleteSingle(this.apiType, stage._id)
            .subscribe((data: Stage) => {
              this.getStages();
              this.notificationService.notify(
                `"${stage.name}" has been deleted.`,
                'OK'
              );
            });
        }
      });
  }

  private _sortByRoster(a, b): any {
    if (a.roster < b.roster) {
      return -1;
    }
    if (a.roster > b.roster) {
      return 1;
    }
    return 0;
  }
}
