import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';

import { DialogComponent } from 'src/app/components/dialog/dialog.component';

@Injectable({
  providedIn: 'root'
})
export class DialogOpenService {
  constructor(private dialog: MatDialog) {}

  open(description: string, message: string): Observable<any> {
    const dialogRef = this.dialog.open(DialogComponent, {
      data: {
        description,
        message
      }
    });

    return dialogRef.afterClosed();
  }
}
