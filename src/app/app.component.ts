import { Component } from '@angular/core';
import { NewUser } from './types';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  currentUser: NewUser;

  title = 'SSBU Scoreboard';

  constructor() {
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
  }
}
