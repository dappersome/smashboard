import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { UserProfileService } from 'src/app/services/user-profile/user-profile.service';

@Component({
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HomeComponent implements OnInit {
  currentUser$ = this.userProfileService.currentActiveUser$;

  navItems = [
    {
      label: 'Players',
      route: '/players',
      disabled: false
    },
    {
      label: 'Characters',
      route: '/characters',
      disabled: false
    },
    {
      label: 'Stages',
      route: '/stages',
      disabled: false
    },
    {
      label: 'Games',
      route: '/games',
      disabled: false
    },
    {
      label: 'Scoreboard',
      route: '/scoreboard',
      disabled: true
    }
  ];

  constructor(private readonly userProfileService: UserProfileService) {}

  ngOnInit() {}
}
